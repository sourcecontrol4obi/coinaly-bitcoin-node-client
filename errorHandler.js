"use strict";
var createError = require('http-errors');

function handleHttpError(err, response, callback) {
  console.log(err)
  if (!callback) {
    throw new Error("no callback for http error handler- check method signature");
  }

  if (err) {
    callback(err, null);
    return true;
  }
  if (!response) {
    callback(createError('no response'), null);
    return true;
  }
  if (response.statusCode !== 200 &&
    response.statusCode !== 201 &&
    response.statusCode !== 204) {
    var error;
    try {
      var errorBody = JSON.parse(response.body);
      error = errorBody.message;
    } catch (ex) {
      error = createError(response.statusCode, response.body);
    }
    callback(error, null);
    return true;
  }
  return false;
}

module.exports.handleHttpError = handleHttpError;
