"use strict";
var ClientBase = require('./ClientBase');

class Client extends ClientBase {

  /**
   * 
   * @param {Object} opts 
   * opts = {
    *   'apiKey'       : apyKey,
    *   'apiSecret'    : apySecret,
    *   'baseApiUri'   : baseApiUri
    * };
   */
  constructor(opts) {
    super(opts);
  }

  /**
   * 
   * @param {String} name wallet name
   * @param {Boolean} test is test wallet
   */
  async createWallet(name, test) {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/wallets', { name, test }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} id wallet id
   * @param {Object} update update body
   */
  async updateWallet(id, update) {
    let promise = new Promise((resolve, reject) => {
      this._putHttp(`/wallets/${id}`, update, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {Object} query 
   */
  async getWallets(query) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/wallets', query, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} id wallet id
   * @param {Object} options additional query options
   */
  async getWallet(id, options) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp(`/wallets/${id}`, options, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} wallet wallet id
   * @param {String} label optional label for address
   */
  async generateAddress(wallet, label = "") {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/addresses', { wallet, label }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {Object} query
   */
  async getAddresses(query) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/addresses', query, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} wallet wallet id
   * @param {Array<Object>} data transaction data
   * @param {Number} fee fee to use for transaction (set to 0 for auto fee)
   * @param {String} comment comment on transaction
   * @param {String} key unique key for monitoring transaction
   */
  async sendTransaction(wallet, data, fee, comment, key) {
    let promise = new Promise((resolve, reject) => {
      this._postHttp('/transactions', { wallet, data, fee, comment, key }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} hash 
   * @param {Boolean} test true if testnet
   */
  async getTransaction(hash, test) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp(`/transactions/${hash}`, { test }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }

  /**
   * 
   * @param {String} wallet wallet id
   * @param {Number} count total count to return
   * @param {Number} skip total count to skip
   */
  async getTransactions(wallet, count, skip) {
    let promise = new Promise((resolve, reject) => {
      this._getHttp('/transactions', { wallet, count, skip }, (err, res) => {
        if (err) reject(err);
        else resolve(res);
      })
    })
    return await promise;
  }
}

module.exports = Client;
