### install
```bash
$ npm install --save coinaly-wallet
```

### Note! This package is still in active development

The following is a list of actions that can be performed using this module
- [x] create wallet
- [x] wallet info (balance checking)
- [x] get payment address
- [x] send transaction
- [x] get transaction(s)

### constructors
Name | Parameters
-----|-----------
default | options: { 'apiKey':String, 'apiSecret':String, 'baseApiUri':String }

### methods
Name | Description | Parameters | Response
-----|-------------|------------|----------
createWallet |  | name:String, notificationUrl:String, test:Boolean | 
updateWallet |  | id:String, update:Object | 
getWallets |  | query: Object |
getWallet |  | wallet:String, options:Object |
getAddress |  | wallet:String |
sendTransaction |  | wallet:String, data:[{ destination, amount }], fee:Number, comment:String, key:String |
getTransaction |  | hash:String, test:Boolean |
getTransactions |  | wallet:String, count:Number, skip:Number |

### usage
```javascript
const CoinalyWallet = require('coinaly-bitcoin-node-client');

//initialization
const Wallet = new CoinalyWallet({
  baseApiUri: 'http://node_api_url',
  apiKey: '***************',
  apiSecret: '***************'
});

// create wallet 
let createWallet = async () => {
    try {
        let response = await coinalyWallet.createWallet('your_domain_wallet1', true|false);
    } catch (e) {
        // handle exception
    }
}

// get wallet info
let getWallets = async () => {
    try {
        let response = await coinalyWallet.getWallets({ test: true });
    } catch (e) {
        // handle exception
    }
}
let getWallet = async () => {
    try {
        let response = await coinalyWallet.getWallet('wallet_id');
    } catch (e) {
        // handle exception
    }
}

// get new payment address
let getAddress = async () => {
    try {
        let response = await coinalyWallet.getAddress('wallet_id');
    } catch (e) {
        // handle exception
    }
}

// send transaction
let sendTransaction = async () => {
    try {
        let data = [
            { destination: 'btc_address_xxxxxxxx1', amount: 0.1 },
            { destination: 'btc_address_xxxxxxxx2', amount: 1.2 },
            ...
        ];
        let response = await coinalyWallet.send('wallet_id', data);
    } catch (e) {
        // handle exception
    }
}
```